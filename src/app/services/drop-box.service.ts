import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Folder} from "../components/gestionnaire-fichier/models/folder";
import {File} from "../components/gestionnaire-fichier/models/file";
import {AbstractElement} from "../components/gestionnaire-fichier/models/element";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {BehaviorSubject} from "rxjs";
import {MesComptesComponent} from "app/components/mes-comptes/mes-comptes.component";
import {SpaceStorageInfoToDropboxComponent} from "../components/mes-comptes/components/connection-div/components/space-storage-info-to-dropbox/space-storage-info-to-dropbox.component";
import {ConnectionButtonToDropboxComponent} from "../components/mes-comptes/components/connection-div/components/connection-button-to-dropbox/connection-button-to-dropbox.component";

@Injectable()
export class DropBoxService {
  /*Urls*/
  private _connectionUrl = "https://cloud-francois.herokuapp.com/dropbox";
  private _getFilesUrl = this._connectionUrl + "/get_files";
  private _getOccupiedSpaceUrl = this._connectionUrl + "/get_used";
  private _getTotalSpaceUrl = this._connectionUrl + "/get_total";
  private _getIsConnectedUrl = this._connectionUrl + "/is_connected";
  private _deconnectionUrl = this._connectionUrl + "/disconnect";

  /*LocalElement*/
  private isLocalConnected: boolean;

  /*DistantElement*/
  private _isDistantConnected: boolean;
  private _occupiedStorageSpaceOnDropbox: number;
  private _totalStorageSpaceOnDropbox: number;

  private elements: Array<AbstractElement>;

  /*BehaviorSubject*/
  private isDistantConnectedBehaviorSubject: BehaviorSubject<boolean>;
  private occupiedStorageSpaceBehaviorSubject: BehaviorSubject<number>;
  private totalStorageSpaceBehaviorSubject: BehaviorSubject<number>;

  constructor(private http: Http) {
    this.isLocalConnected = false;
    this._isDistantConnected = false;
    this._occupiedStorageSpaceOnDropbox = 0;
    this._totalStorageSpaceOnDropbox = 0;

    this.isDistantConnectedBehaviorSubject = new BehaviorSubject(this._isDistantConnected);
    this.occupiedStorageSpaceBehaviorSubject = new BehaviorSubject(this._occupiedStorageSpaceOnDropbox);
    this.totalStorageSpaceBehaviorSubject = new BehaviorSubject(this._totalStorageSpaceOnDropbox);
  }

  public updateConnectedStatus(): void {
    this.http.get(this._getIsConnectedUrl).map((res: Response) => res.json()).catch(this.handleError).subscribe(isDistantConnected => {
      this.isDistantConnected = isDistantConnected;
      this.isLocalConnected = isDistantConnected;
      this.updateAllStorageSpaces();
    });

  }

  public updateAllStorageSpaces(): void {
    this.updateOccupiedStorageSpace();
    this.updateTotalStorageSpace();
  }

  private updateOccupiedStorageSpace(): void {
    if (this.isConnected()) {
      this.http.get(this._getOccupiedSpaceUrl).map((res: Response) => res.json()).catch(this.handleError).subscribe(occupiedStorageSpace => {
        this.occupiedStorageSpaceOnDropbox = parseFloat(occupiedStorageSpace);
      });
    } else {
      this.occupiedStorageSpaceOnDropbox = 0;
    }
  }

  private updateTotalStorageSpace(): void {
    if (this.isConnected()) {
      this.http.get(this._getTotalSpaceUrl).map((res: Response) => res.json()).catch(this.handleError).subscribe(totalStorageSpace => {
        this.totalStorageSpaceOnDropbox = parseFloat(totalStorageSpace);
      });
    } else {
      this.totalStorageSpaceOnDropbox = 0;
    }
  }

  public getElements(): Observable<AbstractElement[]> {
    return this.http.get(this._getFilesUrl).map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    let body: any = res.json();
    this.elements = new Array<AbstractElement>();

    for (let item of body.entries) {
      if (item[".tag"] == File.file_type)
        this.elements.push(new File(item.id, 'dropbox', item.name, item.size, item.path_display + '/', item.path_display.replace(item.name, ''), "Francois"));
      else if (item[".tag"] == Folder.folder_type)
        this.elements.push(new Folder(item.id, 'dropbox', item.name, item.size, item.path_display + '/', item.path_display.replace(item.name, ''), "Francois"));
    }
    return this.elements;
  }

  private handleError(error: Response | any) {
    let errMsg: string;

    errMsg = error.toString();

    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private isConnected(): boolean {
    return this.isLocalConnected && this._isDistantConnected;
  }

  public deconnection(): void {
    this.http.get(this._deconnectionUrl).catch(this.handleError).subscribe();
    this.isLocalConnected = false;
    this.updateConnectedStatus();
  }

  public createRequest(path: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    this.http.put(this.connectionUrl + "/create_folder/" + path, options).catch(this.handleError).subscribe();
  }

  public deleteRequest(id: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    this.http.post(this._connectionUrl + "/delete/" + id, {headers: headers}).catch(this.handleError).subscribe();
  }

  public moveRequest(fromPath: string, toPath: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    this.http.post(this.connectionUrl + "/move/" + fromPath + "/" + toPath, options).catch(this.handleError).subscribe();
  }

  shareRequest(path: string): Observable<string> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    return this.http.get(this.connectionUrl + "/share/" + path, options).map(res => res.text()).catch(this.handleError);
  }

  downloadRequest(path: string): Observable<string> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    return this.http.get(this.connectionUrl + "/download/" + path, options).map(res => res.text()).catch(this.handleError);
  }

  /*Subscriptions*/
  public subscribeMesComptesComponent(subscriber: MesComptesComponent) {
    this.occupiedStorageSpaceBehaviorSubject.subscribe((newOccupiedStorageSpace: number) => {
      subscriber.occupiedStorageSpaceOnDropbox = newOccupiedStorageSpace;
      subscriber.updateSumOfOccupiedStorageSpace();
    });

    this.totalStorageSpaceBehaviorSubject.subscribe((newTotalStorageSpace: number) => {
      subscriber.totalStorageSpaceOnDropbox = newTotalStorageSpace;
      subscriber.updateSumOfTotalStorageSpace();
    });
  }

  public subscribeConnectionButtonToDropboxComponent(subscriber: ConnectionButtonToDropboxComponent) {
    this.isDistantConnectedBehaviorSubject.subscribe(((isDistantConnected: boolean) => {
      subscriber.isConnected = isDistantConnected;
    }));
  }

  public subscribeSpaceStorageInfoToDropboxComponent(subscriber: SpaceStorageInfoToDropboxComponent) {
    this.isDistantConnectedBehaviorSubject.subscribe(((isDistantConnected: boolean) => {
      subscriber.isAvaible = isDistantConnected;
    }));

    this.occupiedStorageSpaceBehaviorSubject.subscribe((newOccupiedStorageSpace: number) => {
      subscriber.occupiedStorageSpaceOnRepository = newOccupiedStorageSpace;
    });

    this.totalStorageSpaceBehaviorSubject.subscribe((newTotalStorageSpace: number) => {
      subscriber.totalStorageSpaceOnRepository = newTotalStorageSpace;
    });
  }

  /*Getters*/
  get connectionUrl(): string {
    return this._connectionUrl;
  }

  get deconnectionUrl(): string {
    this.isLocalConnected = false;
    return this._deconnectionUrl;
  }

  /*Setters*/
  set isDistantConnected(isDistantConnected: boolean) {
    this._isDistantConnected = isDistantConnected;
    this.isDistantConnectedBehaviorSubject.next(isDistantConnected);
  }

  set occupiedStorageSpaceOnDropbox(newOccupiedStorageSpace: number) {
    this._occupiedStorageSpaceOnDropbox = newOccupiedStorageSpace;
    this.occupiedStorageSpaceBehaviorSubject.next(newOccupiedStorageSpace);
  }

  set totalStorageSpaceOnDropbox(newTotalStorageSpace: number) {
    this._totalStorageSpaceOnDropbox = newTotalStorageSpace;
    this.totalStorageSpaceBehaviorSubject.next(newTotalStorageSpace);
  }
}
