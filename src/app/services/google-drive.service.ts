import {Injectable} from "@angular/core";
import {AbstractElement} from "../components/gestionnaire-fichier/models/element";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Folder} from "../components/gestionnaire-fichier/models/folder";
import {File} from "../components/gestionnaire-fichier/models/file";
import {BehaviorSubject} from "rxjs";
import {MesComptesComponent} from "../components/mes-comptes/mes-comptes.component";
import {SpaceStorageInfoToGoogleDriveComponent} from "../components/mes-comptes/components/connection-div/components/space-storage-info-to-google-drive/space-storage-info-to-google-drive.component";
import {ConnectionButtonToGoogleDriveComponent} from "../components/mes-comptes/components/connection-div/components/connection-button-to-google-drive/connection-button-to-google-drive.component";

@Injectable()
export class GoogleDriveService {
  /*Urls*/
  private _connectionUrl = "https://cloud-francois.herokuapp.com/google";
  private _getFilesUrl = this._connectionUrl + "/get_files";
  private _getOccupiedSpaceUrl = this._connectionUrl + "/get_used";
  private _getTotalSpaceUrl = this._connectionUrl + "/get_total";
  private _getIsConnectedUrl = this._connectionUrl + "/is_connected";
  private _deconnectionUrl = this._connectionUrl + "/disconnect";

  /*LocalElement*/
  private isLocalConnected: boolean;

  /*DistantElement*/
  private _isDistantConnected: boolean;
  private _occupiedStorageSpaceOnGoogleDrive: number;
  private _totalStorageSpaceOnGoogleDrive: number;

  private elements: Array<AbstractElement>;

  /*BehaviorSubject*/
  private isDistantConnectedBehaviorSubject: BehaviorSubject<boolean>;
  private occupiedStorageSpaceBehaviorSubject: BehaviorSubject<number>;
  private totalStorageSpaceBehaviorSubject: BehaviorSubject<number>;

  constructor(private http: Http) {
    this.isLocalConnected = false;
    this._isDistantConnected = false;
    this._occupiedStorageSpaceOnGoogleDrive = 0;
    this._totalStorageSpaceOnGoogleDrive = 0;

    this.isDistantConnectedBehaviorSubject = new BehaviorSubject(this._isDistantConnected);
    this.occupiedStorageSpaceBehaviorSubject = new BehaviorSubject(this._occupiedStorageSpaceOnGoogleDrive);
    this.totalStorageSpaceBehaviorSubject = new BehaviorSubject(this._totalStorageSpaceOnGoogleDrive);
  }

  public updateConnectedStatus(): void {
    this.http.get(this._getIsConnectedUrl).map((res: Response) => res.json()).catch(this.handleError).subscribe(isDistantConnected => {
      this.isDistantConnected = isDistantConnected;
      this.isLocalConnected = isDistantConnected;
      this.updateAllStorageSpaces();
    });
  }

  public updateAllStorageSpaces(): void {
    this.updateOccupiedStorageSpace();
    this.updateTotalStorageSpace();
  }

  private updateOccupiedStorageSpace(): void {
    if (this.isConnected()) {
      this.http.get(this._getOccupiedSpaceUrl).map((res: Response) => res.json()).catch(this.handleError).subscribe(occupiedStorageSpace => {
        this.occupiedStorageSpaceOnGoogleDrive = parseFloat(occupiedStorageSpace);
      });
    } else {
      this.occupiedStorageSpaceOnGoogleDrive = 0;
    }
  }

  private updateTotalStorageSpace(): void {
    if (this.isConnected()) {
      this.http.get(this._getTotalSpaceUrl).map((res: Response) => res.json()).catch(this.handleError).subscribe(totalStorageSpace => {
        this.totalStorageSpaceOnGoogleDrive = parseFloat(totalStorageSpace);
      });
    } else {
      this.totalStorageSpaceOnGoogleDrive = 0;
    }
  }

  getElements(): Observable<AbstractElement[]> {
    return this.http.get(this._getFilesUrl).map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    let body: any = res.json();
    this.elements = new Array<AbstractElement>();
    let items: Array<any> = body.entries;
    for (let item of items) {
      let id = item.id;
      let source = 'gdrive';
      let name = item.title;
      let size: number = 0;
      let path = item.id;
      let parentPath = (item.parent.isRoot == true) ? "/" : item.parent.id;
      let owner = "Quentin";

      if (item.kind == "application/vnd.google-apps.document")
        this.elements.push(new File(id, source, name, size, path, parentPath, owner));
      else if (item.kind == "application/vnd.google-apps.folder")
        this.elements.push(new Folder(id, source, name, size, path, parentPath, owner));
    }
    return this.elements;
  }

  private handleError(error: Response | any) {
    let errMsg: string;

    errMsg = error.toString();

    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private isConnected(): boolean {
    return this.isLocalConnected && this._isDistantConnected;
  }

  public deconnection(): void {
    this.http.get(this._deconnectionUrl).catch(this.handleError).subscribe();
    this.isLocalConnected = false;
    this.updateConnectedStatus();
  }

  public createRequest(path: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    this.http.put(this.connectionUrl + "/create_folder/" + path, options).catch(this.handleError).subscribe();
  }

  public deleteRequest(id: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    this.http.post(this._connectionUrl + "/delete/" + id, {headers: headers}).catch(this.handleError).subscribe();
  }

  public moveRequest(fromPath: string, toPath: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    this.http.post(this.connectionUrl + "/move/" + fromPath + "/" + toPath, options).catch(this.handleError).subscribe();
  }

  public shareRequest(path: string): Observable<string> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    return this.http.get(this.connectionUrl + "/share/" + path, options).map(res => res.text()).catch(this.handleError);
  }

  public downloadRequest(path: string): Observable<string> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({headers: headers});

    return this.http.get(this.connectionUrl + "/download/" + path, options).map(res => res.text()).catch(this.handleError);
  }

  /*Subscriptions*/
  public subscribeMesComptesComponent(subscriber: MesComptesComponent) {
    this.occupiedStorageSpaceBehaviorSubject.subscribe((newOccupiedStorageSpace: number) => {
      subscriber.occupiedStorageSpaceOnGoogleDrive = newOccupiedStorageSpace;
      subscriber.updateSumOfOccupiedStorageSpace();
    });

    this.totalStorageSpaceBehaviorSubject.subscribe((newTotalStorageSpace: number) => {
      subscriber.totalStorageSpaceOnGoogleDrive = newTotalStorageSpace;
      subscriber.updateSumOfTotalStorageSpace();
    });
  }

  public subscribeConnectionButtonToGoogleDriveComponent(subscriber: ConnectionButtonToGoogleDriveComponent) {
    this.isDistantConnectedBehaviorSubject.subscribe(((isDistantConnected: boolean) => {
      subscriber.isConnected = isDistantConnected;
    }));
  }

  public subscribeSpaceStorageInfoToGoogleDriveComponent(subscriber: SpaceStorageInfoToGoogleDriveComponent) {
    this.isDistantConnectedBehaviorSubject.subscribe(((isDistantConnected: boolean) => {
      subscriber.isAvaible = isDistantConnected;
    }));

    this.occupiedStorageSpaceBehaviorSubject.subscribe((newOccupiedStorageSpace: number) => {
      subscriber.occupiedStorageSpaceOnRepository = newOccupiedStorageSpace;
    });

    this.totalStorageSpaceBehaviorSubject.subscribe((newTotalStorageSpace: number) => {
      subscriber.totalStorageSpaceOnRepository = newTotalStorageSpace;
    });
  }

  /*Getters*/
  get connectionUrl(): string {
    return this._connectionUrl;
  }

  get deconnectionUrl(): string {
    this.isLocalConnected = false;
    return this._deconnectionUrl;
  }

  /*Setters*/
  set isDistantConnected(isDistantConnected: boolean) {
    this._isDistantConnected = isDistantConnected;
    this.isDistantConnectedBehaviorSubject.next(isDistantConnected);
  }

  set occupiedStorageSpaceOnGoogleDrive(newOccupiedStorageSpace: number) {
    this._occupiedStorageSpaceOnGoogleDrive = newOccupiedStorageSpace;
    this.occupiedStorageSpaceBehaviorSubject.next(newOccupiedStorageSpace);
  }

  set totalStorageSpaceOnGoogleDrive(newTotalStorageSpace: number) {
    this._totalStorageSpaceOnGoogleDrive = newTotalStorageSpace;
    this.totalStorageSpaceBehaviorSubject.next(newTotalStorageSpace);
  }
}
