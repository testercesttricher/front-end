import {inject, TestBed} from "@angular/core/testing";

import {DropBoxService} from "./drop-box.service";

describe('DropBoxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DropBoxService]
    });
  });

  it('should ...', inject([DropBoxService], (service: DropBoxService) => {
    expect(service).toBeTruthy();
  }));
});
