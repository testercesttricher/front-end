import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {RouterModule, Routes} from "@angular/router";

import {AppComponent} from "./app.component";
import {MenuComponent} from "./components/menu/menu.component";
import {AccueilComponent} from "./components/accueil/accueil.component";
import {ElementSelectedService} from "./components/gestionnaire-fichier/services/element-selected.service";
import {ElementListComponent} from "./components/gestionnaire-fichier/components/element-list/element-list.component";
import {ElementDetailsComponent} from "./components/gestionnaire-fichier/components/element-details/element-details.component";
import {MesComptesComponent} from "./components/mes-comptes/mes-comptes.component";
import {GestionnaireFichierComponent} from "./components/gestionnaire-fichier/gestionnaire-fichier.component";
import {RepositoryIdPipe} from "./components/gestionnaire-fichier/pipe/repository-id.pipe";
import {DropBoxService} from "./services/drop-box.service";
import {GoogleDriveService} from "./services/google-drive.service";
import {ConnectionDivComponent} from "./components/mes-comptes/components/connection-div/connection-div.component";
import {ConnectionButtonToGoogleDriveComponent} from "./components/mes-comptes/components/connection-div/components/connection-button-to-google-drive/connection-button-to-google-drive.component";
import {ConnectionButtonToDropboxComponent} from "./components/mes-comptes/components/connection-div/components/connection-button-to-dropbox/connection-button-to-dropbox.component";
import {ConnectionButtonToOneDriveComponent} from "./components/mes-comptes/components/connection-div/components/connection-button-to-one-drive/connection-button-to-one-drive.component";
import {MenuService} from "./components/menu/services/menu.service";
import {SpaceStorageInfoToDropboxComponent} from "./components/mes-comptes/components/connection-div/components/space-storage-info-to-dropbox/space-storage-info-to-dropbox.component";
import {SpaceStorageInfoToGoogleDriveComponent} from "./components/mes-comptes/components/connection-div/components/space-storage-info-to-google-drive/space-storage-info-to-google-drive.component";
import {SpaceStorageInfoToOneDriveComponent} from "./components/mes-comptes/components/connection-div/components/space-storage-info-to-one-drive/space-storage-info-to-one-drive.component";
import {MdButtonModule, MdCheckboxModule, MdDialogModule, MdInputModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AddDialogComponent} from "./components/gestionnaire-fichier/components/add-dialog/add-dialog.component";
import {DeleteDialogComponent} from "./components/gestionnaire-fichier/components/delete-dialog/delete-dialog.component";
import {MoveDialogComponent} from "./components/gestionnaire-fichier/components/move-dialog/move-dialog.component";
import {ShareDialogComponent} from "./components/gestionnaire-fichier/components/share-dialog/share-dialog.component";

const appRoutes: Routes = [
  {path: AccueilComponent.ROUTE_PATH, component: AccueilComponent},
  {path: MesComptesComponent.ROUTE_PATH, component: MesComptesComponent},
  {path: GestionnaireFichierComponent.ROUTE_PATH, component: GestionnaireFichierComponent},
  {path: '', redirectTo: AccueilComponent.ROUTE_PATH, pathMatch: 'full'},
  {path: '**', redirectTo: AccueilComponent.ROUTE_PATH, pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AccueilComponent,
    ElementListComponent,
    ElementDetailsComponent,
    MesComptesComponent,
    GestionnaireFichierComponent,
    RepositoryIdPipe,
    ConnectionDivComponent,
    ConnectionButtonToGoogleDriveComponent,
    ConnectionButtonToDropboxComponent,
    ConnectionButtonToOneDriveComponent,
    SpaceStorageInfoToDropboxComponent,
    SpaceStorageInfoToGoogleDriveComponent,
    SpaceStorageInfoToOneDriveComponent,
    AddDialogComponent,
    DeleteDialogComponent,
    MoveDialogComponent,
    ShareDialogComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpModule,
    MdCheckboxModule,
    MdDialogModule,
    BrowserAnimationsModule,
    MdInputModule,
    MdButtonModule
  ],
  providers: [
    ElementSelectedService,
    MenuService,
    GoogleDriveService,
    DropBoxService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddDialogComponent,
    DeleteDialogComponent,
    MoveDialogComponent,
    ShareDialogComponent]
})

export class AppModule {


}
