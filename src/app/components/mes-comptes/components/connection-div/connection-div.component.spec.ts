import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {ConnectionDivComponent} from "./connection-div.component";

describe('ConnectionDivComponent', () => {
  let component: ConnectionDivComponent;
  let fixture: ComponentFixture<ConnectionDivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionDivComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
