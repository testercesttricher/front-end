import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: 'app-connection-div',
  templateUrl: './connection-div.component.html',
  styleUrls: ['./connection-div.component.css']
})
export class ConnectionDivComponent implements OnInit {

  /*IMG paramaters*/
  @Input("img-src") imgSrc: string;
  @Input("img-alt") imgAlt: string;
  IMG_SIZE: number = 128; // it's used in .html as a constant

  constructor() {

  }

  ngOnInit() {
  }
}
