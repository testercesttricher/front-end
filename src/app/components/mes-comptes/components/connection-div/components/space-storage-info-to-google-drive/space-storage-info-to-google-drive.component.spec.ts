import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {SpaceStorageInfoToGoogleDriveComponent} from "./space-storage-info-to-google-drive.component";

describe('SpaceStorageInfoToGoogleDriveComponent', () => {
  let component: SpaceStorageInfoToGoogleDriveComponent;
  let fixture: ComponentFixture<SpaceStorageInfoToGoogleDriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpaceStorageInfoToGoogleDriveComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceStorageInfoToGoogleDriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
