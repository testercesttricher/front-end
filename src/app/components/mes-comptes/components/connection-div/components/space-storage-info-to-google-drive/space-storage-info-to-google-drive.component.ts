import {Component} from "@angular/core";
import {AbstractSpaceStrorageInfoComponent} from "../abstract-space-strorage-info/abstract-space-strorage-info.component";
import {GoogleDriveService} from "../../../../../../services/google-drive.service";

@Component({
  selector: 'app-space-storage-info-to-google-drive',
  templateUrl: '../abstract-space-strorage-info/abstract-space-strorage-info.component.html',
  styleUrls: ['../abstract-space-strorage-info/abstract-space-strorage-info.component.css']
})
export class SpaceStorageInfoToGoogleDriveComponent extends AbstractSpaceStrorageInfoComponent {
  isAvaible: boolean;

  occupiedStorageSpaceOnRepository: number;
  totalStorageSpaceOnRepository: number;

  constructor(private googleDriveService: GoogleDriveService) {
    super();
  }

  ngOnInit() {
    this.googleDriveService.subscribeSpaceStorageInfoToGoogleDriveComponent(this);
    this.googleDriveService.updateAllStorageSpaces();
  }

}
