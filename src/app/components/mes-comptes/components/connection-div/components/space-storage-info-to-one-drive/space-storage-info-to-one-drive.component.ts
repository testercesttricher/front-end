import {Component} from "@angular/core";
import {AbstractSpaceStrorageInfoComponent} from "../abstract-space-strorage-info/abstract-space-strorage-info.component";

@Component({
  selector: 'app-space-storage-info-to-one-drive',
  templateUrl: '../abstract-space-strorage-info/abstract-space-strorage-info.component.html',
  styleUrls: ['../abstract-space-strorage-info/abstract-space-strorage-info.component.css']
})
export class SpaceStorageInfoToOneDriveComponent extends AbstractSpaceStrorageInfoComponent {
  isAvaible: boolean;

  occupiedStorageSpaceOnRepository: number;
  totalStorageSpaceOnRepository: number;

  constructor() {
    super();
  }

  ngOnInit() {
    this.isAvaible = false;
  }

}
