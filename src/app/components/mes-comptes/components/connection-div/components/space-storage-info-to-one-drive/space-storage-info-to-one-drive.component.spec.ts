import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {SpaceStorageInfoToOneDriveComponent} from "./space-storage-info-to-one-drive.component";

describe('SpaceStorageInfoToOneDriveComponent', () => {
  let component: SpaceStorageInfoToOneDriveComponent;
  let fixture: ComponentFixture<SpaceStorageInfoToOneDriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpaceStorageInfoToOneDriveComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceStorageInfoToOneDriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
