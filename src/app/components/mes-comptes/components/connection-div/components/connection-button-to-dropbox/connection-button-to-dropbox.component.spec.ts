import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {ConnectionButtonToDropboxComponent} from "./connection-button-to-dropbox.component";

describe('ConnectionButtonToDropboxComponent', () => {
  let component: ConnectionButtonToDropboxComponent;
  let fixture: ComponentFixture<ConnectionButtonToDropboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionButtonToDropboxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionButtonToDropboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
