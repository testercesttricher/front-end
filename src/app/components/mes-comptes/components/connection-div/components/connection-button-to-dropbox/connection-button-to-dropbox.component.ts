import {Component} from "@angular/core";
import {AbstractConnectionButtonComponent} from "../abstract-connection-button/abstract-connection-button.component";
import {DropBoxService} from "../../../../../../services/drop-box.service";

@Component({
  selector: 'app-connection-button-to-dropbox',
  templateUrl: '../abstract-connection-button/abstract-connection-button.component.html',
  styleUrls: ['../abstract-connection-button/abstract-connection-button.component.css']
})
export class ConnectionButtonToDropboxComponent extends AbstractConnectionButtonComponent {
  repositoryName: string = "Dropbox";
  isConnected: boolean;

  constructor(private dropboxService: DropBoxService) {
    super();
  }

  ngOnInit() {
    this.dropboxService.subscribeConnectionButtonToDropboxComponent(this);
    this.dropboxService.updateConnectedStatus();
  }

  connectionMethod(): void {
    this.openConnectionUrl(this.dropboxService.connectionUrl);
    window.onfocus = () => {
      this.dropboxService.updateConnectedStatus();
      window.onfocus = () => {
      };
    };
  }

  deconnectionMethod(): void {
    this.dropboxService.deconnection();
  }

}
