import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {ConnectionButtonToGoogleDriveComponent} from "./connection-button-to-google-drive.component";

describe('ConnectionButtonToGoogleDriveComponent', () => {
  let component: ConnectionButtonToGoogleDriveComponent;
  let fixture: ComponentFixture<ConnectionButtonToGoogleDriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionButtonToGoogleDriveComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionButtonToGoogleDriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
