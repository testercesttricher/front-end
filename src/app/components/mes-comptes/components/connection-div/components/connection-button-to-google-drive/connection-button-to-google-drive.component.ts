import {Component} from "@angular/core";
import {AbstractConnectionButtonComponent} from "../abstract-connection-button/abstract-connection-button.component";
import {GoogleDriveService} from "../../../../../../services/google-drive.service";

@Component({
  selector: 'app-connection-button-to-google-drive',
  templateUrl: '../abstract-connection-button/abstract-connection-button.component.html',
  styleUrls: ['../abstract-connection-button/abstract-connection-button.component.css']
})
export class ConnectionButtonToGoogleDriveComponent extends AbstractConnectionButtonComponent {
  repositoryName: string = "Google Drive";
  isConnected: boolean;

  constructor(private googleDriveService: GoogleDriveService) {
    super();
  }

  ngOnInit() {
    this.googleDriveService.subscribeConnectionButtonToGoogleDriveComponent(this);
    this.googleDriveService.updateConnectedStatus();
  }

  connectionMethod(): void {
    this.openConnectionUrl(this.googleDriveService.connectionUrl);
    window.onfocus = () => {
      this.googleDriveService.updateConnectedStatus();
      window.onfocus = () => {
      };
    };
  }

  deconnectionMethod(): void {
    this.googleDriveService.deconnection();
  }

}
