import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {ConnectionButtonToOneDriveComponent} from "./connection-button-to-one-drive.component";

describe('ConnectionButtonToOneDriveComponent', () => {
  let component: ConnectionButtonToOneDriveComponent;
  let fixture: ComponentFixture<ConnectionButtonToOneDriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionButtonToOneDriveComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionButtonToOneDriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
