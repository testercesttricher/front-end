import {Component} from "@angular/core";
import {AbstractConnectionButtonComponent} from "../abstract-connection-button/abstract-connection-button.component";

@Component({
  selector: 'app-connection-button-to-one-drive',
  //templateUrl: '../abstract-connection-button/abstract-abstract-connection-button.component.html',
  templateUrl: 'connection-button-to-one-drive.component.html',
  styleUrls: ['../abstract-connection-button/abstract-connection-button.component.css']
})
export class ConnectionButtonToOneDriveComponent extends AbstractConnectionButtonComponent {
  repositoryName: string = "OneDrive";
  isConnected: boolean;

  constructor() {
    super();
  }

  ngOnInit() {
    this.isConnected = false;
  }

  connectionMethod(): void {
  }

  deconnectionMethod(): void {
  }

}
