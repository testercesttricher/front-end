import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {AbstractSpaceStrorageInfoComponent} from "./abstract-space-strorage-info.component";

describe('AbstractSpaceStrorageInfoComponent', () => {
  let component: AbstractSpaceStrorageInfoComponent;
  let fixture: ComponentFixture<AbstractSpaceStrorageInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AbstractSpaceStrorageInfoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractSpaceStrorageInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
