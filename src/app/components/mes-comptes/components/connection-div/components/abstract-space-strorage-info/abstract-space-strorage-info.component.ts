import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'app-abstract-space-strorage-info',
  templateUrl: './abstract-space-strorage-info.component.html',
  styleUrls: ['./abstract-space-strorage-info.component.css']
})
export abstract class AbstractSpaceStrorageInfoComponent implements OnInit {
  abstract isAvaible: boolean;

  abstract occupiedStorageSpaceOnRepository: number;
  abstract totalStorageSpaceOnRepository: number;

  constructor() {
  }

  ngOnInit() {
  }

}
