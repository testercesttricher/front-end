import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'app-abstract-connection-button',
  templateUrl: './abstract-connection-button.component.html',
  styleUrls: ['./abstract-connection-button.component.css']
})
export abstract class AbstractConnectionButtonComponent implements OnInit {
  abstract repositoryName: string;
  abstract isConnected: boolean;

  ngOnInit() {
  }

  abstract connectionMethod(): void ;

  abstract deconnectionMethod(): void ;

  openConnectionUrl(connectionUrl: string) {
    let popUpHeight: number = (window.innerHeight * 0.80);
    let popUpWidth: number = (window.innerWidth * 0.80);
    let top: number = (window.outerHeight - popUpHeight) / 2;
    let left: number = (window.outerWidth - popUpWidth) / 2;

    let connectionWindow: Window = window.open(
      connectionUrl,
      '_blank',
      'height=' + popUpHeight + ', width=' + popUpWidth + ', top=' + top + ', left=' + left + ', scrollbars=yes, status=yes');
    connectionWindow.focus();
  }
}
