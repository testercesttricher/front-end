import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {SpaceStorageInfoToDropboxComponent} from "./space-storage-info-to-dropbox.component";

describe('SpaceStorageInfoToDropboxComponent', () => {
  let component: SpaceStorageInfoToDropboxComponent;
  let fixture: ComponentFixture<SpaceStorageInfoToDropboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpaceStorageInfoToDropboxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceStorageInfoToDropboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
