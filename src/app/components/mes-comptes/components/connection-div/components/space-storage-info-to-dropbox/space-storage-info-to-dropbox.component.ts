import {Component} from "@angular/core";
import {AbstractSpaceStrorageInfoComponent} from "../abstract-space-strorage-info/abstract-space-strorage-info.component";
import {DropBoxService} from "../../../../../../services/drop-box.service";

@Component({
  selector: 'app-space-storage-info-to-dropbox',
  templateUrl: '../abstract-space-strorage-info/abstract-space-strorage-info.component.html',
  styleUrls: ['../abstract-space-strorage-info/abstract-space-strorage-info.component.css']
})
export class SpaceStorageInfoToDropboxComponent extends AbstractSpaceStrorageInfoComponent {
  isAvaible: boolean;
  occupiedStorageSpaceOnRepository: number;
  totalStorageSpaceOnRepository: number;

  constructor(private dropboxService: DropBoxService) {
    super();
  }

  ngOnInit() {
    this.dropboxService.subscribeSpaceStorageInfoToDropboxComponent(this);
    this.dropboxService.updateAllStorageSpaces();
  }

}
