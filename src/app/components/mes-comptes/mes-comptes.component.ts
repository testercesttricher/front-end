import {Component, OnInit} from "@angular/core";
import {MenuService} from "../menu/services/menu.service";
import {GoogleDriveService} from "../../services/google-drive.service";
import {DropBoxService} from "../../services/drop-box.service";

@Component({
  selector: 'app-connexion',
  templateUrl: './mes-comptes.component.html',
  styleUrls: ['./mes-comptes.component.css']
})
export class MesComptesComponent implements OnInit {

  static ROUTE_PATH: string = "comptes";

  /*CONNECTION_DIV parameters*/
  googleDriveIconSrc: string = "src/assets/img/googleDriveIcon.png";
  googleDriveIconAlt: string = "Google Drive icon";

  DropboxIconSrc: string = "src/assets/img/dropboxIcon.png";
  DropboxIconAlt: string = "Dropbox icon";

  oneDriveIconSrc: string = "src/assets/img/OneDriveIconV2.png";
  oneDriveIconAlt: string = "OneDrive icon";

  /*STORAGE_STORE information*/
  occupiedStorageSpaceOnGoogleDrive: number;
  totalStorageSpaceOnGoogleDrive: number;

  occupiedStorageSpaceOnDropbox: number;
  totalStorageSpaceOnDropbox: number;

  occupiedStorageSpaceOnOneDrive: number;
  totalStorageSpaceOnOneDrive: number;

  sumOfOccupiedStorageSpace: number;
  sumOfTotalStorageSpace: number;

  constructor(private menuService: MenuService,
              private googleDriveService: GoogleDriveService,
              private dropboxService: DropBoxService) {
    this.sumOfOccupiedStorageSpace = 0;
    this.sumOfTotalStorageSpace = 0;
  }

  ngOnInit() {
    this.menuService.moveFocusOnMesComptes();

    this.googleDriveService.subscribeMesComptesComponent(this);
    this.dropboxService.subscribeMesComptesComponent(this);
  }

  updateSumOfOccupiedStorageSpace(): void {
    this.sumOfOccupiedStorageSpace = 0;
    this.sumOfOccupiedStorageSpace += this.occupiedStorageSpaceOnGoogleDrive;
    this.sumOfOccupiedStorageSpace += this.occupiedStorageSpaceOnDropbox;
    //this.sumOfOccupiedStorageSpace += this.occupiedStorageSpaceOnOneDrive;
  }

  updateSumOfTotalStorageSpace(): void {
    this.sumOfTotalStorageSpace = 0;
    this.sumOfTotalStorageSpace += this.totalStorageSpaceOnGoogleDrive;
    this.sumOfTotalStorageSpace += this.totalStorageSpaceOnDropbox;
    //this.sumOfTotalStorageSpace += this.totalStorageSpaceOnOneDrive;
  }

}
