import {inject, TestBed} from "@angular/core/testing";

import {ElementSelectedService} from "./element-selected.service";

describe('ElementSelectedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElementSelectedService]
    });
  });

  it('should ...', inject([ElementSelectedService], (service: ElementSelectedService) => {
    expect(service).toBeTruthy();
  }));
});
