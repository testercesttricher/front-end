import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {AbstractElement} from "../models/element";
import {ElementDetailsComponent} from "../components/element-details/element-details.component";

@Injectable()
export class ElementSelectedService {
  private _elementSelected: AbstractElement;
  private subject: BehaviorSubject<AbstractElement>;

  constructor(){
    this.subject = new BehaviorSubject<AbstractElement>(this.elementSelected);
  }

  public subscribe(subscriber: ElementDetailsComponent) {
    this.subject.subscribe(newSelectedElement => subscriber.elementSelected = newSelectedElement)
  }

  get elementSelected(): AbstractElement {
    return this._elementSelected;
  }

  set elementSelected(newSelectedElement: AbstractElement) {
    this._elementSelected = newSelectedElement;
    this.subject.next(newSelectedElement);
  }
}
