import {Component} from "@angular/core";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: 'app-dialog-box',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.css']
})
export class AddDialogComponent {
  dropbox: boolean;
  gdrive: boolean;
  onedrive: boolean;
  path: string;

  constructor(public dialogRef: MdDialogRef<AddDialogComponent>) {
  }

  getPath() {
    if (this.dropbox)
      return ['dropbox', encodeURIComponent(this.path)];
    if (this.gdrive)
      return ['gdrive', encodeURIComponent(this.path)];
  }

}
