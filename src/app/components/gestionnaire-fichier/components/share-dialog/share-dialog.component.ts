import {Component, OnInit} from "@angular/core";
import {MdDialogRef} from "@angular/material";
import {DropBoxService} from "../../../../services/drop-box.service";
import {AbstractElement} from "../../models/element";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-share-dialog',
  templateUrl: './share-dialog.component.html',
  styleUrls: ['./share-dialog.component.css']
})
export class ShareDialogComponent implements OnInit {

  element: AbstractElement;
  link: string;
  title: string;
  type: string;

  constructor(public dialogRef: MdDialogRef<ShareDialogComponent>,
              public service: DropBoxService,
              public sanitizer: DomSanitizer) {
    this.link = "";
  }

  ngOnInit() {
    let str = encodeURIComponent(this.element.path.slice(0, -1));

    if (this.type = 'share') {
      this.service.shareRequest(str).subscribe(path => this.link = path.replace('"', '').replace('"', ''));
    }
    else if (this.type = 'download') {
      this.service.downloadRequest(str).subscribe(path => this.link = path.replace('"', '').replace('"', ''));
    }
  }

  sanitize(url: string): any {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
