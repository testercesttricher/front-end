import {Component} from "@angular/core";
import {MdDialogRef} from "@angular/material";
import {AbstractElement} from "../../models/element";

@Component({
  selector: 'app-move-dialog',
  templateUrl: './move-dialog.component.html',
  styleUrls: ['./move-dialog.component.css']
})
export class MoveDialogComponent {

  element: AbstractElement;
  toPath: string;

  constructor(public dialogRef: MdDialogRef<MoveDialogComponent>) {
  }

  getData() {
    return [encodeURIComponent(this.element.path.slice(0, -1)), encodeURIComponent(this.toPath)];
  }

}
