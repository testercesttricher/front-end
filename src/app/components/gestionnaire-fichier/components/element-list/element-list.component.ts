import {Component, OnInit} from "@angular/core";
import {ElementSelectedService} from "../../services/element-selected.service";
import {AbstractElement} from "../../models/element";
import {DropBoxService} from "../../../../services/drop-box.service";
import {GoogleDriveService} from "../../../../services/google-drive.service";
import {MdDialog, MdDialogConfig} from "@angular/material";
import {AddDialogComponent} from "../add-dialog/add-dialog.component";
import {DeleteDialogComponent} from "../delete-dialog/delete-dialog.component";
import {MoveDialogComponent} from "../move-dialog/move-dialog.component";
import {ShareDialogComponent} from "../share-dialog/share-dialog.component";

@Component({
  selector: 'element-list',
  templateUrl: './element-list.component.html',
  styleUrls: ['./element-list.component.css'],
  providers: [DropBoxService, GoogleDriveService]
})
export class ElementListComponent implements OnInit {
  dropboxElements: Array<AbstractElement>;
  gdriveElements: Array<AbstractElement>;
  listElements: Array<AbstractElement>;
  elementSelected: AbstractElement;
  private _parentRepositoryId: Array<string>;
  private _repositoryId: string;
  result: Array<string>;

  gdrive: boolean = false;
  dropbox: boolean = false;
  onedrive: boolean = false;

  constructor(private elementStateService: ElementSelectedService,
              private dropboxService: DropBoxService,
              private gdriveService: GoogleDriveService,
              public dialog: MdDialog) {
    this.dropboxElements = [];
    this.gdriveElements = [];
    this.listElements = [];
    this._parentRepositoryId = new Array<string>();
    this._repositoryId = "/";
    this.result = [];
  }

  ngOnInit() {
    this.getElements();
  }

  private getElements(): void {
    this.dropboxService.getElements().subscribe(data => this.dropboxElements = data);
    this.gdriveService.getElements().subscribe(data => this.gdriveElements = data);
  }

  onSelect(element: AbstractElement): void {
    this.elementSelected = element;
    this.elementStateService.elementSelected = this.elementSelected;
  }

  onChecking(): void {
    if (this.gdrive && this.dropbox)
      this.merge();
    else if (this.gdrive)
      this.listElements = this.gdriveElements;
    else if (this.dropbox)
      this.listElements = this.dropboxElements;
    else
      this.listElements = [];
    this._repositoryId = '/';
    this.resetElementSelected();
  }

  resetElementSelected(): void {
    this.elementSelected = null;
    this.elementStateService.elementSelected = null;
  }

  goBack(): void {
    this._repositoryId = this._parentRepositoryId.pop();
    this.resetElementSelected();
  }

  goForward(value: string): void {
    this._parentRepositoryId.push(this.repositoryId);
    this._repositoryId = value;
    this.resetElementSelected();
  }

  merge() {
    let a = this.dropboxElements.slice(0);
    for (let item1 in this.gdriveElements) {
      for (let item2 in a) {
        if (a[item2].type == 'folder' && this.gdriveElements[item1].name == a[item2].name) {
          a.splice(parseInt(item2, 10), 1);
        }
      }
    }
    this.listElements = a.concat(this.gdriveElements);
  }

  addElement() {
    let config = new MdDialogConfig();
    let dialogRef = this.dialog.open(AddDialogComponent, config);

    dialogRef.componentInstance.dropbox = this.dropbox;
    dialogRef.componentInstance.gdrive = this.gdrive;
    dialogRef.componentInstance.onedrive = this.onedrive;

    dialogRef.afterClosed().subscribe(res => {
      if (res != false) {
        if (res[0] == 'dropbox')
          this.dropboxService.createRequest(res[1]);
      }
    })
  }

  deleteElement() {
    let config = new MdDialogConfig();
    let dialogRef = this.dialog.open(DeleteDialogComponent, config);

    dialogRef.componentInstance.element = this.elementSelected;

    dialogRef.afterClosed().subscribe(id => {
      if (id != false) {
        if (this.elementSelected.source == 'dropbox')
          this.dropboxService.deleteRequest(id);
      }
    });
  }

  moveElement() {
    let config = new MdDialogConfig();
    let dialogRef = this.dialog.open(MoveDialogComponent, config);

    dialogRef.componentInstance.element = this.elementSelected;

    dialogRef.afterClosed().subscribe(res => {
      if (res != false) {
        if (this.elementSelected.source == 'dropbox')
          this.dropboxService.moveRequest(res[0], res[1]);
      }

    })
  }

  shareElement() {
    let config = new MdDialogConfig();
    let dialogRef = this.dialog.open(ShareDialogComponent, config);

    dialogRef.componentInstance.element = this.elementSelected;
    dialogRef.componentInstance.title = "Partager un fichier";
    dialogRef.componentInstance.type = 'share';

    dialogRef.afterClosed().subscribe();
  }

  downloadElement() {
    let config = new MdDialogConfig();
    let dialogRef = this.dialog.open(ShareDialogComponent, config);

    dialogRef.componentInstance.element = this.elementSelected;
    dialogRef.componentInstance.title = "Télécharger un fichier";
    dialogRef.componentInstance.type = 'download';

    dialogRef.afterClosed().subscribe();
  }

  /*Getters & Setters*/

  get repositoryId(): string {
    return this._repositoryId;
  }

  set repositoryId(value: string) {
    this._parentRepositoryId.push(this.repositoryId);
    this._repositoryId = value;
  }

}
