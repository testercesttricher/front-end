import {Component, OnInit} from "@angular/core";
import {ElementSelectedService} from "../../services/element-selected.service";
import {AbstractElement} from "../../models/element";

@Component({
  selector: 'element-details',
  templateUrl: './element-details.component.html',
  styleUrls: ['./element-details.component.css']
})
export class ElementDetailsComponent implements OnInit {

  elementSelected: AbstractElement;

  constructor(private elementSelectedService: ElementSelectedService) {

  }

  ngOnInit() {
    this.subscribeToElementSelectedService();
  }

  /* Permet au selectedFile d'être mis à jour pour
   toujours correspondre au fichier selectionné */
  private subscribeToElementSelectedService(): void {
    this.elementSelectedService.subscribe(this);
  }

}
