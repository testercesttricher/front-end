import {Component} from "@angular/core";
import {MdDialogRef} from "@angular/material";
import {AbstractElement} from "../../models/element";

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent {

  element: AbstractElement;

  constructor(public dialogRef: MdDialogRef<DeleteDialogComponent>) {
  }

  getPath() {
    return encodeURIComponent(this.element.path.slice(0, -1));
  }


}
