import {Pipe, PipeTransform} from "@angular/core";
import {AbstractElement} from "../models/element";

@Pipe({
  name: 'repositoryId'
})
export class RepositoryIdPipe implements PipeTransform {

  transform(listElements: Array<AbstractElement>, repositoryId: string): any {
    return listElements.filter((element: AbstractElement, index: number, listElement: Array<AbstractElement>) => {
      return (repositoryId == element.parentPath);
    });
  }

}
