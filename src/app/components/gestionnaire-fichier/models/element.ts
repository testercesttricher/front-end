export abstract class AbstractElement {
  private _id: string;
  private _source: string;
  private _name: string;
  private _type: string;
  private _size: number;
  private _path: string;
  private _parentPath: string;
  private _owner: string;

  constructor(id: string, source: string, name: string, type: string, size: number, path: string, parentPath: string, owner: string) {
    this._id = id;
    this._source = source;
    this._name = name;
    this._type = type;
    this._size = size;
    this._path = path;
    this._parentPath = parentPath;
    this._owner = owner;
  }

  get id(): string {
    return this._id;
  }

  get source(): string {
    return this._source;
  }

  get name(): string {
    return this._name;
  }

  get type(): string {
    return this._type;
  }

  get size(): number {
    return this._size;
  }

  get path(): string {
    return this._path;
  }

  get parentPath(): string {
    return this._parentPath;
  }

  get owner(): string {
    return this._owner;
  }
}
