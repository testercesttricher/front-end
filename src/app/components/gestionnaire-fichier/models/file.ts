import {AbstractElement} from "./element";

export class File extends AbstractElement {
  static file_type: string = "file";

  constructor(id: string, source: string, name: string, size: number, path: string, parentPath: string, owner: string) {
    super(id, source, name, File.file_type, size, path, parentPath, owner);
  }
}
