import {AbstractElement} from "./element";

export class Folder extends AbstractElement {
  static folder_type: string = "folder";

  constructor(id: string, source: string, name: string, size: number, path: string, parentPath: string, owner: string) {
    super(id, source, name, Folder.folder_type, size, path, parentPath, owner);
  }
}
