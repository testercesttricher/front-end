import {Component, OnInit} from "@angular/core";
import {MenuService} from "../menu/services/menu.service";

@Component({
  selector: 'app-gestionnaire-fichier',
  templateUrl: './gestionnaire-fichier.component.html',
  styleUrls: ['./gestionnaire-fichier.component.css']
})
export class GestionnaireFichierComponent implements OnInit {

  static ROUTE_PATH: string = "fichiers";

  constructor(private menuService: MenuService) {
  }

  ngOnInit() {
    this.menuService.moveFocusOnMesFichiers();
  }

}
