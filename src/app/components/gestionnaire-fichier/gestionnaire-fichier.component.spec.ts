import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {GestionnaireFichierComponent} from "./gestionnaire-fichier.component";

describe('GestionnaireFichierComponent', () => {
  let component: GestionnaireFichierComponent;
  let fixture: ComponentFixture<GestionnaireFichierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GestionnaireFichierComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireFichierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
