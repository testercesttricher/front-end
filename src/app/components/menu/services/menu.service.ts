import {Injectable} from "@angular/core";
import {MenuItem} from "../models/menu-item.enum";
import {BehaviorSubject} from "rxjs";
import {MenuComponent} from "../menu.component";

@Injectable()
export class MenuService {

  private menuItemsClassBehaviorSubject: BehaviorSubject<Array<String>>;

  private _menuItemsClass: Array<String>;

  constructor() {
    this._menuItemsClass = [
      "", //ACCUEIL_CSS_CLASS       = 0
      "", //MES_COMPTES_CSS_CLASS   = 1
      ""  //MES_FICHIERS_CSS_CLASS  = 2
    ];

    this.menuItemsClassBehaviorSubject = new BehaviorSubject<Array<String>>(this.menuItemsClass);
  }

  public subscribe(subscriber: MenuComponent) {
    this.menuItemsClassBehaviorSubject.subscribe(newMenuItemsClass => {
      subscriber.menuItemsClass = newMenuItemsClass;
    })
  }

  get menuItemsClass(): Array<String> {
    return this._menuItemsClass;
  }

  set menuItemsClass(newMenuItemsClass: Array<String>) {
    this._menuItemsClass = newMenuItemsClass;
    this.menuItemsClassBehaviorSubject.next(newMenuItemsClass);
  }

  moveFocusOnAccueil() {
    this.resetItemSelection();

    let newMenuItemsClass: Array<String> = this.menuItemsClass.slice();
    newMenuItemsClass[MenuItem.ACCUEIL_CSS_CLASS] = "active";

    this.menuItemsClass = newMenuItemsClass;
  }

  moveFocusOnMesComptes() {
    this.resetItemSelection();

    let newMenuItemsClass: Array<String> = this.menuItemsClass.slice();
    newMenuItemsClass[MenuItem.MES_COMPTES_CSS_CLASS] = "active";

    this.menuItemsClass = newMenuItemsClass;
  }

  moveFocusOnMesFichiers() {
    this.resetItemSelection();

    let newMenuItemsClass: Array<String> = this.menuItemsClass.slice();
    newMenuItemsClass[MenuItem.MES_FICHIERS_CSS_CLASS] = "active";

    this.menuItemsClass = newMenuItemsClass;
  }

  resetItemSelection(): void {
    let newMenuItemsClass: Array<String> = this.menuItemsClass.slice();

    for (let i: number = 0; i < newMenuItemsClass.length; i++) {
      newMenuItemsClass[i] = "";
    }

    this.menuItemsClass = newMenuItemsClass;
  }


}
