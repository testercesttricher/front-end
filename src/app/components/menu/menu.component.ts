import {Component, OnInit} from "@angular/core";
import {MenuService} from "./services/menu.service";
import {AccueilComponent} from "../accueil/accueil.component";
import {MesComptesComponent} from "../mes-comptes/mes-comptes.component";
import {GestionnaireFichierComponent} from "../gestionnaire-fichier/gestionnaire-fichier.component";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  menuItemsClass: Array<String>;

  menuItemsPath: Array<String> = [
    AccueilComponent.ROUTE_PATH,
    MesComptesComponent.ROUTE_PATH,
    GestionnaireFichierComponent.ROUTE_PATH
  ];

  constructor(private menuService: MenuService) {

  }

  ngOnInit() {
    this.subscribeToMenuService();
  }

  /* Permet d'avoir continuellement à jour les class bootstrap,
   * des items du menu, présent dans "menuItemsClass" */
  subscribeToMenuService(): void {
    this.menuService.subscribe(this);
  }

}
