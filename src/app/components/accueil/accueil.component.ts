import {Component, OnInit} from "@angular/core";
import {MenuService} from "../menu/services/menu.service";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  static ROUTE_PATH: string = "accueil";

  constructor(private menuService: MenuService) {
  }

  ngOnInit() {
    this.menuService.moveFocusOnAccueil();
  }

}
